
.global cmpr
cmpr:
    //int $0x80
    push %rbp;
    mov %rsp,%rbp
    mov %rdi,%rbx
    mov %rsi, %rdx
    xor %rcx, %rcx
    // Проверяем указатели и первый байт на 0
    cmpq %rbx, %rcx
        je error
    cmpq %rdx, %rcx
        je error
    cmpb (%rbx), %cl
        je error
    cmpb (%rdx), %cl
        je error
    loop:
        xor %rax, %rax
        movb (%rbx), %al
        movb (%rdx), %ah
        // Символы не равны друг другу
        cmpb %al, %ah
            jne not_equals
        // Достигли конца строки
        cmpb %al, %cl
            je equals
        mov $0x20, %ch
        cmpb %al, %ch
            je equals
        inc %rbx
        inc %rdx
        jmp loop

equals:
    mov $0, %rax
    pop %rbp
    ret

not_equals:
    //mov $0, %rax
    cmpw $0x2000, %ax
        je are_spaces
    cmp $32, %ax
        je are_spaces
    pop %rbp
    ret
are_spaces:
    jmp equals


error:
    mov $-1, %rax
    pop %rbp
    ret

.global no_signs
no_signs:
    push %rbp
    mov %rsp,%rbp
    mov %rdi,%rbx
    movb $0x20, %ah
    dec %rbx
loop_2:
    inc %rbx
    movb (%rbx), %al
    cmpb $0, %al
        je no_signs_end
    cmp $0x2f, %al
        jg gt_2f
    jmp no_signs_replace
gt_2f:
    cmp $0x3a, %al
        jl loop_2
    cmp $0x40, %al
        jg gt_40
    jmp no_signs_replace
gt_40:
    cmp $0x5b, %al
        jl loop_2
    cmp $0x60, %al
        jg gt_60
    jmp no_signs_replace
gt_60:
    cmp $0x7b, %al
        jl loop_2
    jmp no_signs_replace

no_signs_end:
    pop %rbp
    ret

no_signs_replace:
    movb %ah, (%rbx)
    jmp loop_2