#include <iostream>
#include <cstring>
#include <unistd.h>

extern "C" unsigned int cmp(char *, char *);
extern "C" void no_signs(char *);
extern "C" char *next_word(char *, int=0);


int main() {
    char b[] = "The calling convention of the System V AMD64 ABI is followed on Solaris, Linux, FreeBSD, macOS,[18] and"
            " is the de facto standard among Unix and Unix-like operating systems. The first six integer or pointer"
            " arguments are passed in registers RDI, RSI, RDX, RCX, R8, R9 ";
    no_signs(b);
    char *c = b;
    while (*c) {
        int count = 0;
        char *c_b = b;
        while (*c_b != 0) {
            if (cmp(c_b,  c) == 0) {
                if(c_b < c) {
                    break;
                }
                count++;
            }
            c_b = next_word(c_b);
        }
        c = next_word(c, count);
    }
    return 0;
}