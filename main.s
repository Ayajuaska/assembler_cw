text_input:
    .ascii "The calling convention of the System V AMD64 ABI is followed on "
    .ascii "Solaris, Linux, FreeBSD, macOS,[18] and is the de facto standard "
    .ascii "among Unix and Unix-like operating systems. "
    .ascii "The first six integer or pointer arguments are passed in "
    .asciz "registers RDI, RSI, RDX, RCX, R8, R9 "
text_end:
    .byte 0
text_len:
    .quad (text_input - text_end + 1)

.macro save_registers
    push %rax
    push %rbx
    push %rcx
    push %rdx
.endm

.macro load_registers
    pop %rdx
    pop %rcx
    pop %rbx
    pop %rax
.endm

.global main
main:
    pushq %rbp
    mov %rsp, %rbp
    jmp copy_str
str_ready:
    movq text_len, %rax
    leaq (%rbp,%rax), %rdi
    call no_signs
    movq text_len, %rax
    leaq (%rbp,%rax), %rax
while_first:
mov $0x0, %rdx
cmpb (%rax), %dl
jz return
    mov text_len, %rbx
    leaq (%rbp,%rbx), %rbx
    mov $0x0, %rcx
while_second:
xor %rdx, %rdx
cmpb (%rbx), %dl
jz end_while_2
    save_registers
    mov %rbx, %rdi
    mov %rax, %rsi
    call cmpr
    mov $0, %rsi
    mov %eax, %esi
    load_registers
    cmp $0, %rsi
        jne next_word_int
    cmp %rbx, %rax
        jg end_while_2
    inc %rcx
next_word_int:
    //
    save_registers
    mov %rbx, %rdi
    mov $0, %rsi
    call next_word
    mov %rax, %rdi
    load_registers
    mov %rdi, %rbx
jmp while_second
end_while_2:
    save_registers
    mov %rax, %rdi
    mov %rcx, %rsi
    call next_word
    mov %rax, %rdi
    load_registers
    mov %rdi, %rax
    jmp while_first
return:
    //popq %rbp
    leave
    mov $0, %rax
    ret

copy_str:
   mov $text_input, %rax
   mov %rbp, %rbx
   mov $0, %rcx
   //sub $8, %rbx
   rewind_loop:
      cmp (%rax), %cx
      je before_cpy
      inc %rax
      jmp rewind_loop
   before_cpy:
      mov %rax, %rdx
      sub $text_input, %rdx
      sub %rdx, %rsp
      //sub $8, %rsp

   cpy_loop:
        cmp $text_input, %rax
        jl __end__
        movb (%rax), %dl
        movb %dl, (%rbx)
        dec %rbx
        dec %rax
    jmp cpy_loop
__end__:
   inc %rbx
   jmp str_ready
