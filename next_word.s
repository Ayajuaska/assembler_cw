fmt:
    .asciz ": %d\n"

.global next_word
next_word:
    push %rbp
while_zero_or_space:
    movb $0x20, %al
    cmpb %al, (%rdi)
    je end_while
    movb $0x0, %al
    cmpb %al, (%rdi)
    je end_while
    #cycle body:
    ## if count
    cmp $0x0, %rsi
    jne put_char
after_put_char:
    inc %rdi
    jmp while_zero_or_space
end_while:

    ## if count
    cmp $0x0, %rsi
    jne print_it

while_space:
    movb $0x20, %al
    cmpb %al, (%rdi)
    jne finish
    inc %rdi
    jmp while_space
finish:
    mov %rdi, %rax
    pop %rbp
    ret

print_it:
    mov $0x0, %rax
    push %rdi
    mov $fmt, %rdi
    call printf
    pop %rdi
    jmp while_space

put_char:
    push %rdi
    push %rsi
    mov %rdi, %rsi
    mov $0x1, %rdi
    mov $0x1, %rdx
    mov $0x1, %rax
    syscall
    pop %rsi
    pop %rdi
    jmp after_put_char
